package dealer.zipcodes.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import dealer.zipcodes.DealerModel;
import dealer.zipcodes.service.DealerZipCodesService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dealerszipcodes")
public class DealerZipCodesController {
	
	private final DealerZipCodesService dealerZipCodesService;
	
	public DealerZipCodesController(DealerZipCodesService dealerZipCodesService) {
		this.dealerZipCodesService = dealerZipCodesService;
	}
	
	/** Find dealers within radius of miles from given zipcode by calling ZipCodes Service
	 *  to get zipcodes and then query dealars database to find dealers
	 * @param zipCode zipCode
	 * @param minimumRadius minimumRadius
	 * @param maximumRadius maximumRadius
	 * @return List of dealers within radius from given zipcode by calling ZipCodes Service
	 */
	@RequestMapping(value = "/dealersWithinRadiusFromZipCode", method = RequestMethod.GET)
	@ApiOperation(value = "Find dealers within radius from given zipcode by calling ZipCodes Service to get zipcodes and then query dealars database to find dealers")
	public ResponseEntity<List<DealerModel>> zipCodesWithinRadiusFromZipCode__ZipCodeService(@RequestParam("zipCode") String zipCode,@RequestParam("minimumRadius") String minimumRadius,@RequestParam("maxRadius") String maximumRadius){
		List<DealerModel> dealers = null;		
		try {
			dealers = dealerZipCodesService.dealersWithinRadiusByZipCode(zipCode, minimumRadius, maximumRadius);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<DealerModel>>(dealers, HttpStatus.OK);
		
	}
	
	/**
	 * Find dealers within radius from given latitude and longitude by calling ZipCodes Service
	 * to get zipcodes and then query dealars database to find dealers
	 * @param latitude latitude
	 * @param longitude longitude
	 * @param radius radius
	 * @return List of dealers within radius from given latitude and longitude by calling ZipCodes Service
	 */
	@RequestMapping(value = "/dealersWithinRadiusFromLatitudeAndLongitude", method = RequestMethod.GET)
	@ApiOperation(value = "Find dealers within radius from given latitude and longitude by calling ZipCodes Service to get zipcodes and then query dealars database to find dealers")
	public ResponseEntity<List<DealerModel>> zipCodesWithinRadiusFromLatitudeAndLongitude_ZipCodeService(@RequestParam("latitude") double latitude, @RequestParam("longitude") double longitude , @RequestParam("radius") int radius){
		
		List<DealerModel> dealers = null;	
		try {
			dealers = dealerZipCodesService.dealersWithinRadiusByLatitudeAndLongitude(latitude, longitude, radius);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<DealerModel>>(dealers, HttpStatus.OK);
	}
	
	
	/**
	 * Find dealers within radius from given zipcode from localDatabase ZipCodes and Dealers
	 * @param zipCode zipcode
	 * @param radius radius in miles
	 * @return List of dealers within radius from given zipcode from localDatabase ZipCodes and Dealers
	 */
	@RequestMapping(value = "/dealersWithinRadiusLocalDB", method = RequestMethod.GET)
	@ApiOperation(value = "Find dealers within radius from given zipcode from localDatabase ZipCodes and Dealers")
	public ResponseEntity<List<DealerModel>> zipCodesWithinRadius_LocalDB(@RequestParam("zipCode") String zipCode,@RequestParam("radius") int radius){
		List<DealerModel> dealers = null;
		try {
			dealers = dealerZipCodesService.dealersWithinRadiusByZipCode_LocalDB(zipCode, radius);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<List<DealerModel>>(dealers, HttpStatus.OK);
		
	}

	/**Find Dealers within radius from given latitude and longitude from localDatabase ZipCodes and Dealers
	 * @param latitude latitude
	 * @param longitude longitude
	 * @param radius radius in miles
	 * @return List of dealers within radius from given latitude and longitude from localDatabase ZipCodes and Dealers
	 */
	@RequestMapping(value = "/dealersWithinRadiusLatitudeAndLongitudeLocalDB", method = RequestMethod.GET)
	@ApiOperation(value = "Find Dealers within radius from given latitude and longitude from localDatabase Zipcodes")
	public ResponseEntity<List<DealerModel>> dealersWithinRadiusByLatitudeAndLongitude_LocalDB(@RequestParam("latitude") double latitude, @RequestParam("longitude") double longitude , @RequestParam("radius") int radius ){
		
		List<DealerModel> dealers = null;
		try {
			dealers = dealerZipCodesService.dealersWithinRadiusByLatitudeAndLongitude_LocalDB(latitude, longitude, radius);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		return new ResponseEntity<List<DealerModel>>(dealers, HttpStatus.OK);
	}

}
