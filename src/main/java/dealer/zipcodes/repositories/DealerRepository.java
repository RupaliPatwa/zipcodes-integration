package dealer.zipcodes.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import dealer.zipcodes.domain.Dealer;

/**
 * DealerRepository
 * For Dealers Table
 *
 */
public interface DealerRepository  extends PagingAndSortingRepository<Dealer,Long>{
	
	String query = "select dealer_id,dealer_name,zip from dealers where zip in (:zipcodes)";
	
	@Query(value = query,nativeQuery=true )
	public List<Dealer> getDealersWithinRadiusWithGivenZipCodes(@Param("zipcodes") List<String> zipCodes);
	
}
