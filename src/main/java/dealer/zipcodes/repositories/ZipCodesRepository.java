package dealer.zipcodes.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import dealer.zipcodes.domain.ZipCodes;

/**
 * ZipCodesRepository
 * For ZipCodes table
 *
 */
public interface ZipCodesRepository  extends PagingAndSortingRepository<ZipCodes,Long>{
	
	String query = "SELECT zipcode,latitude,longitude from (select distinct zipcode,latitude,longitude ,( 3959 * acos( cos( radians(:latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians( latitude ) ) ) ) " + 
			"AS distance FROM zipcodes) as t where distance <:radius";
	
	@Query(value = query,nativeQuery=true )
	public List<ZipCodes> zipCodesWithinRadiusFromLatitudeAndLongitude(@Param("latitude") double latitude, @Param("longitude") double longitude, @Param("radius") int radius);
	
	public ZipCodes findByzipcode(String zipcode);

}
