package dealer.zipcodes;

import java.math.BigInteger;

import lombok.Data;

@Data
public class DealerModel {
	
	private BigInteger dealer_id;
	private String dealerName;
	private String zip;

}
