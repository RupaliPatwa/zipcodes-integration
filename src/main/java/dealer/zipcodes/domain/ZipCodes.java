package dealer.zipcodes.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "zipcodes")
@Data
@EqualsAndHashCode
public class ZipCodes {
	
	@Id
	@Column(name = "ZIPCODE", nullable = false)
	private String zipcode;
	
	@Column(name = "LATITUDE", nullable = true)
	private double latitude;
	
	@Column(name = "LONGITUDE", nullable = true)
	private double longitude;

}
