package dealer.zipcodes.domain;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "Dealers")
@Data
@EqualsAndHashCode
public class Dealer {
	
	@Id
	@Column(name = "dealer_id", nullable = false)
	private BigInteger dealer_id;

	@Column(name = "dealer_name", nullable = false)
	private String dealerName;
	
	@Column(name = "ZIP", nullable = false)
	private String zip;
	

}
