package dealer.zipcodes.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import dealer.zipcodes.DealerModel;


/**
 * DealerZipCodesService
 *
 */
public interface DealerZipCodesService {
	
	public String findDistanceBetweenTwoZipCodes(String zipCode1, String zipCode2) throws JsonProcessingException, IOException;
	
	/**
	 * Find Dealers within radius of miles from given zipcode by calling ZipCodes Service and then query dealers database to find dealers
	 * @param zipCode zipCode
	 * @param minimumRadius minimumRadius
	 * @param maximumRadius maximumRadius
	 * @return List of dealers
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<DealerModel> dealersWithinRadiusByZipCode(String zipCode, String minimumRadius, String maximumRadius) throws JsonProcessingException, IOException;
	
	
	/**Find dealers within radius from given latitude and longitude by calling ZipCodes Service  and then query dealers database to find dealers
	 * @param latitude latitude
	 * @param longitude longitude
	 * @param radius radius
	 * @return  List of dealers
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<DealerModel> dealersWithinRadiusByLatitudeAndLongitude(double latitude, double longitude, int radius) throws JsonProcessingException, IOException;
	
	/**
	 * Find dealers within radius from given zipcode from localDatabase Zipcodes and Dealers
	 * @param zipCode zipCode
	 * @param radius radius
	 * @return List of Zipcodes within radius from given zipcode 
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<DealerModel> dealersWithinRadiusByZipCode_LocalDB(String zipCode,int radius) throws JsonProcessingException, IOException;
	
	/**Find dealers within radius from given latitude and  longitude from localDatabase Zipcodes and Dealers
	 * @param latitude latitude
	 * @param longitude longitude
	 * @param radius radius
	 * @return List of Zipcodes within radius from given latitude and  longitude 
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<DealerModel> dealersWithinRadiusByLatitudeAndLongitude_LocalDB(double latitude, double longitude, int radius) throws JsonProcessingException, IOException;


}
