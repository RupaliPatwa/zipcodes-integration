package dealer.zipcodes.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import dealer.zipcodes.DealerModel;
import dealer.zipcodes.domain.Dealer;
import dealer.zipcodes.domain.ZipCodes;
import dealer.zipcodes.repositories.DealerRepository;
import dealer.zipcodes.repositories.ZipCodesRepository;

@Service
public class DealerZipCodesServiceImpl implements DealerZipCodesService{

	
	@Autowired
	ZipCodesRepository zipcodeRepository;
	
	@Autowired
	DealerRepository dealerRepository;
	
	public static final String UTF8_BOM = "\uFEFF";
	private final String API_KEY ="DEMOAPIKEY";
	
	private final String ZIPCODE_DISTANCE_URL = "https://api.zip-codes.com/ZipCodesAPI.svc/1.0/CalculateDistance/ByZip";
	private final String ZIPCODES_WITHINRADIUS_URL = "https://api.zip-codes.com/ZipCodesAPI.svc/1.0/FindZipCodesInRadius";
	private final String ZIPCODES_WITHINRADIUS_LONGITUDEANDLATITUDE_URL = "https://api.zip-codes.com/ZipCodesAPI.svc/1.0/FindZipCodesInRadius/ByLatLon";
	private final RestTemplate restTemplate;
	private final ObjectMapper mapper;
	
	public DealerZipCodesServiceImpl(RestTemplateBuilder restTemplateBuilder, ObjectMapper mapper) {
        this.mapper = mapper;        
		this.restTemplate = restTemplateBuilder.build();
	}
	
	@Override
	public String findDistanceBetweenTwoZipCodes(String fromZipCode, String toZipcode) throws JsonProcessingException, IOException {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ZIPCODE_DISTANCE_URL).
										queryParam("fromzipcode", fromZipCode).
										queryParam("tozipcode", toZipcode).queryParam("key", API_KEY);

    	ResponseEntity<String> response = restTemplate.getForEntity(uriBuilder.build().toUri(), String.class);
    	String jsonResponse = response.getBody().trim();
    	jsonResponse = removeUTF8BOM(jsonResponse);
    	JsonNode root = mapper.readTree(jsonResponse);
    	return root.path("DistanceInMiles").asText();

	}

	private static String removeUTF8BOM(String s) {
	    if (s.startsWith(UTF8_BOM)) {
	        s = s.substring(1);
	    }
	    return s;
	}

	/* (non-Javadoc)
	 * @see dealer.zipcodes.service.DealerZipCodesService#dealersWithinRadiusByZipCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<DealerModel> dealersWithinRadiusByZipCode(String zipCode, String minimumRadius, String maximumRadius) throws JsonProcessingException, IOException {
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ZIPCODES_WITHINRADIUS_URL).
				queryParam("zipcode", zipCode).
				queryParam("minimumradius", 0).
				queryParam("maximumradius", maximumRadius).
				queryParam("key", API_KEY);
		
		ResponseEntity<String> response = restTemplate.getForEntity(uriBuilder.build().toUri(), String.class);
    	String jsonResponse = response.getBody().trim();
    	jsonResponse = removeUTF8BOM(jsonResponse);
    	JsonNode root = mapper.readTree(jsonResponse);
    	List<String> zipcodes = new ArrayList<String>();
    	root.path("DataList").forEach(element -> {
    			zipcodes.add(element.path("Code").asText());
	    });
		
    	List<DealerModel> dealers = getDealersWithinRadiusWithGivenZipCodes(zipcodes);
		return dealers;
	}
	
	/* (non-Javadoc)
	 * @see zipcodes.service.ZipCodesService#zipCodesWithinRadiusFromLatitudeAndLongitude(double, double, int)
	 */
	@Override
	public List<DealerModel> dealersWithinRadiusByLatitudeAndLongitude(double latitude, double longitude, int radius)
			throws JsonProcessingException, IOException {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ZIPCODES_WITHINRADIUS_LONGITUDEANDLATITUDE_URL).
				queryParam("latitude", latitude).
				queryParam("longitude", longitude).
				queryParam("minimumradius", 0).
				queryParam("maximumradius", radius).
				queryParam("key", API_KEY);
		ResponseEntity<String> response = restTemplate.getForEntity(uriBuilder.build().toUri(), String.class);
    	String jsonResponse = response.getBody().trim();
    	jsonResponse = removeUTF8BOM(jsonResponse);
    	JsonNode root = mapper.readTree(jsonResponse);
    	List<String> zipcodes = new ArrayList<String>();
    	root.path("DataList").forEach(element -> {
    		zipcodes.add(element.path("Code").asText());
	    });
		
    	List<DealerModel> dealers = getDealersWithinRadiusWithGivenZipCodes(zipcodes);
		return dealers;
	}

	/* (non-Javadoc)
	 * @see zipcodes.service.ZipCodesService#zipCodesWithinRadiusLocalDB(java.lang.String, int)
	 */
	@Override
	public List<DealerModel> dealersWithinRadiusByZipCode_LocalDB(String zipCode,int radius)
			throws JsonProcessingException, IOException {
		
		//Find latitude and longitude of given Zip from ZipCodes database
		ZipCodes zipCodes =  zipcodeRepository.findByzipcode(zipCode);
		
		List<DealerModel> dealerModels =  dealersWithinRadiusByLatitudeAndLongitude_LocalDB(zipCodes.getLatitude(), zipCodes.getLongitude(), radius);
		return dealerModels;
		
	}

	/* (non-Javadoc)
	 * @see zipcodes.service.ZipCodesService#zipCodesWithinRadiusByLatitudeAndLongitude(double, double, int)
	 */
	@Override
	public List<DealerModel> dealersWithinRadiusByLatitudeAndLongitude_LocalDB(double latitude, double longitude, int radius)
			throws JsonProcessingException, IOException {
		
		//find zipcodes within radius from given latitude and longitude
		List<ZipCodes> zipCodes = zipcodeRepository.zipCodesWithinRadiusFromLatitudeAndLongitude(latitude, longitude, radius);
		
		List<String> zipCodesStrList = new ArrayList<>();
		for(ZipCodes zipCodes2 :  zipCodes) {
			zipCodesStrList.add(zipCodes2.getZipcode());
		}
		
		List<DealerModel> dealers = getDealersWithinRadiusWithGivenZipCodes(zipCodesStrList);
		return dealers;
	}
	
	private List<DealerModel> getDealersWithinRadiusWithGivenZipCodes(List<String> zipCodes){
		
		//find dealers matching zipcodes within radius
		List<Dealer> dealersList =  dealerRepository.getDealersWithinRadiusWithGivenZipCodes(zipCodes);
		
		List<DealerModel> dealerModels = new ArrayList<DealerModel>();
		for(Dealer dealer : dealersList) {
			DealerModel dealerModel = new DealerModel();
			dealerModel.setDealer_id(dealer.getDealer_id());
			dealerModel.setDealerName(dealer.getDealerName());
			dealerModel.setZip(dealer.getZip());
			dealerModels.add(dealerModel);
		}
		return dealerModels;
	}

}
