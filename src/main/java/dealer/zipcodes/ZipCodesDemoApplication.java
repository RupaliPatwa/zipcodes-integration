package dealer.zipcodes;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class ZipCodesDemoApplication {

	
	
	public static void main(String[] args) {
		SpringApplication.run(ZipCodesDemoApplication.class, args);
	}
	@Autowired
    DataSource dataSource;
	
    @Autowired
   private Environment env;
	

}
